import asyncio
import os
import re
import requests
from telethon import TelegramClient, events
from dotenv import dotenv_values
from telethon import utils
import tidalapi

def exit_program(text: str):
    print(text)
    exit(1)

config = dotenv_values(".env")
api_id = config["API_ID"] or exit_program("API_ID is not set")
api_hash = config["API_HASH"] or exit_program("API_HASH is not set")

channel_id = config["CHANNEL_ID"] or exit_program("CHANNEL_ID is not set")

session_id = config["SESSION_ID"] if "SESSION_ID" in config else None
token_type = config["TOKEN_TYPE"] if "TOKEN_TYPE" in config else None
access_token = config["ACCESS_TOKEN"] if "ACCESS_TOKEN" in config else None
refresh_token = config["REFRESH_TOKEN"] if "REFRESH_TOKEN" in config else None

session = tidalapi.Session()

if session_id is None or token_type is None or access_token is None or refresh_token is None:
    session.login_oauth_simple()
    with open(".env", "a") as env_file:
        env_file.write("\n")
        env_file.write(f"SESSION_ID={session.session_id}\n")
        env_file.write(f"TOKEN_TYPE={session.token_type}\n")
        env_file.write(f"ACCESS_TOKEN={session.access_token}\n")
        env_file.write(f"REFRESH_TOKEN={session.refresh_token}\n")
else:
    done = session.load_oauth_session(session_id, token_type, access_token, refresh_token)
    if done is not True:
        session.login_oauth_simple()

client = TelegramClient("session_name", api_id, api_hash).start()


real_id, peer_type = utils.resolve_id(int(channel_id))

peer = peer_type(real_id)

search_reg = re.compile("/search ([\w\s]*?)")
@client.on(events.NewMessage(pattern=search_reg))
async def search(event):
    try:
        if event.message.peer_id.channel_id is peer.channel_id:
            return
    except:
        return
    match = search_reg.fullmatch(event.message.message)
    query = match.group(1)
    results = session.search(query=query, models=[tidalapi.album.Album], limit=10)
    ids = [event.message.id]
    for entry_album in results['albums']:
        album: tidalapi.album.Album = entry_album
        message = await client.send_message(peer, message=f"{album.artist.name}-{album.name}\nId: {album.id}")
        ids.append(message.id)
    await asyncio.sleep(10)
    await client.delete_messages(peer, ids)

dwnl_reg = re.compile("/download ([0-9]*?)")
@client.on(events.NewMessage(pattern=dwnl_reg))
async def downloader(event):
    try:
        if event.message.peer_id.channel_id is peer.channel_id:
            return
    except:
        return
    match = dwnl_reg.fullmatch(event.message.message)
    id = match.group(1)
    album = tidalapi.album.Album(session, id)
    try:
        thumb_name = "thumbnail.jpg"
        url = album.image(160)
        resp = requests.get(url)
        with open(thumb_name, "wb") as thumb:
            thumb.write(resp.content)
        await client.send_file(peer, thumb_name, caption=f"{album.artist.name}-{album.name}")
        os.remove(thumb_name)
    except:
        pass
    items = album.items()
    folder_name = str(album.id)
    try:
        os.mkdir(folder_name)
    except:
        pass
    for item in items:
        if not isinstance(item, tidalapi.media.Track):
            continue
        params = {
            'urlusagemode': 'STREAM',
            'audioquality' : 'HI_RES',
            'assetpresentation': 'FULL',
        }
        request = item.requests.request('GET', 'tracks/%s/urlpostpaywall' % item.id, params)

        resp = requests.get(request.json()['urls'][0])
        song_path = f'{folder_name}/{item.name}-{item.artist.name}.flac'
        with open(song_path, "wb") as file:
            file.write(resp.content)
        await client.send_file(peer, song_path, voice_note=True)
        os.remove(song_path)
    os.removedirs(folder_name)
    await client.delete_messages(peer, [event.message.id])

try:
    print('(Press Ctrl+C to stop this)')
    client.run_until_disconnected()
finally:
    client.disconnect()
