# tidal_telegram

A bot that uploads albums from tidal to a telegram channel.

Setup all of your env variables in .env
You only have to setup APP_ID, APP_HASH and CHANNEL_ID, the ones for tidal can be setup automatically by this for the next usage.

## Piracy
I do not encourage piracy. You respond for what you download.

## Licensing
Since tidalapi is under GPLv3, this will be also under GPLv3.